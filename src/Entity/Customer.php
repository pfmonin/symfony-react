<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CustomerRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

// -- API -- //
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Entity(repositoryClass=CustomerRepository::class)
 * 
 * @ApiResource(
 *  normalizationContext={"groups"={"customer_read"}},
 * 
 *  collectionOperations={"GET" ={"path"="/customers"} , "POST"} , 
 *  itemOperations={"GET" ={"path"="/customers/{id}"}, "PUT", "DELETE"} ,
 * 
 *  subresourceOperations = {
 *      "invoices_get_subresource"={ "path"="/customers/{id}/factures" }
 *  } 
 * )
 * @ApiFilter(SearchFilter::class, properties={"firstName":"partial", "lastName", "company"})
 * @ApiFilter(OrderFilter::class)
 * 
 */
class Customer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * 
     *@Groups({"customer_read", "invoices_read"})
     */
    private $id;

    /**
     *@ORM\Column(type="string", length=255)
     *@Groups({"customer_read", "invoices_read"})
     *@Assert\NotBlank(message="Le prénom du customer est obligatoire")
     *@Assert\Length(min=3, minMessage="Le prénom du customer doit contenir au minimum 3 caractères", max=255, minMessage="Le prénom du customer doit contenir au maximum 255 caractères")
     */
    private $firstName;

    /**
     *@ORM\Column(type="string", length=255)
     *@Groups({"customer_read", "invoices_read"})
     *@Assert\NotBlank(message="Le nom du customer est obligatoire")
     *@Assert\Length(min=3, minMessage="Le nom du customer doit contenir au minimum 3 caractères", max=255, minMessage="Le prénom du customer doit contenir au maximum 255 caractères")
     * 
     */
    private $lastName;

    /**
     *@ORM\Column(type="string", length=255)
     *@Groups({"customer_read", "invoices_read"})
     *@Assert\NotBlank(message="L'adresse mail du customer est obligatoire")
     *@Assert\Email(message="L'adresse mail du customer doit etre valide !")
     */
    private $email;

    /**
     *@ORM\Column(type="string", length=255, nullable=true)
     *@Groups({"customer_read"})
     *
     * 
     */
    private $company;

    /**
     *@ORM\OneToMany(targetEntity=Invoice::class, mappedBy="customer")
     *@Groups({"customer_read"})
     *
     *@ApiSubresource 
     */
    private $invoices;

    /**
     *@ORM\ManyToOne(targetEntity=User::class, inversedBy="customers")
     *@Groups({"customer_read"})
     *@Assert\NotBlank(message="Le user est obligatoire")

     * 
     */
    private $user;

    public function __construct()
    {
        $this->invoices = new ArrayCollection();
    }

    /**
     * Calcul le montant total des invoices 
     * @Groups({"customer_read"})
     * 
     * @return float
     */
    public function getTotalAmount():float
    {
        return array_reduce($this->invoices->toArray(), function($total, $invoice)
        {
            return $total + $invoice->getAmount();
            
        }, 0);
    }

    /**
     * Recupere le montant total hors facture payé ou annulé
     * 
     * @Groups({"customer_read"})
     * 
     * @return float
     */
    public function getUnpaidAmount()
    {
        return array_reduce($this->invoices->toArray(), function ($total, $invoices){
            return $total + ($invoices->getStatus() === "PAID" || $invoices->getStatus()==="CANCELLED" ? 0 : $invoices->getAmount());
        }, 0);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|Invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setCustomer($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->contains($invoice)) {
            $this->invoices->removeElement($invoice);
            // set the owning side to null (unless already changed)
            if ($invoice->getCustomer() === $this) {
                $invoice->setCustomer(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}

import React, {useEffect, useState} from "react"; 
import Pagination from "../components/Pagination";
import customersAPI from "../services/customersAPI";

const CustomerPage = props => { 

    const [customers, setCustomers] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState("");


    // Search action 
    const filteredCustomers = customers.filter( c => 
        c.firstName.toLowerCase().includes(search.toLowerCase()) || 
        c.lastName.toLowerCase().includes(search.toLowerCase()) ||
        c.email.toLowerCase().includes(search.toLowerCase()) ||
        c.company.toLowerCase().includes(search.toLowerCase())

        )
    const handleSearch = event => {
        const value  = event.currentTarget.value;
        setSearch(value);
        setCurrentPage(1);
    }


    // Delete action
    const handleDelete = async id => {   
        const originalCustomers = [...customers];
        setCustomers(customers.filter(customer => customer.id !== id))

        try {
            await customersAPI.delete(id)
        } catch (error) {           
            setCustomers(originalCustomers)
            console.log(error.response, "n'a pas ete supprime")
        }    
    }

    // Get all customers   
    const fetchCustomers = async () => {
        try{
            const data = await customersAPI.findAll()
            setCustomers(data)
        } catch (error) {
            console.log(error.response)
        } 
    }
      
    useEffect( () => {
        fetchCustomers();            
    }, []);

   

    // params pagination
    const handleActivePage = page => [
        setCurrentPage(page)
    ];
    const itemsPerPage = 10;
    const paginatedCustomers = Pagination.getData(filteredCustomers, currentPage, itemsPerPage) 
    

    return (
        <>

        <h1> La page des clients </h1>

        <div className="form-group">
            <input type="text" className="form-control" placeholder="Recherchez..." onChange={handleSearch} value={search} />
        </div>

        <table className="table table-hover">
            <thead>
                <tr>
                <th>Id</th>
                <th>Client</th>
                <th>Email</th>
                <th>Entreprise</th>
                <th className="text-center">Factures</th>
                <th className="text-center">Montant Total</th>
                <th></th>
                </tr>
            </thead>

            <tbody>

                {paginatedCustomers.map(customer => 

                        <tr key={customer.id}>
                            <td>{customer.id}</td>
                            <td>
                                <a href="#" className=""> {customer.lastName} {customer.firstName}  </a>
                            </td>
                            <td>{customer.email}</td>
                            <td> {customer.company} </td>
                            <td className="text-center">
                                <span className=" badge badge-lg badge-primary"> {customer.invoices.length} </span>
                            </td>
                            <td className="text-center"> {customer.totalAmount.toLocaleString()} € </td>
                            <td> 
                                <button onClick={() => handleDelete(customer.id)}
                                        disabled={customer.invoices.length > 0} 
                                        className="btn btn-sm btn-danger">
                                            Supprimer
                                </button> 
                            </td>
                        </tr>
                    )}
            </tbody>
        </table>

        {itemsPerPage < filteredCustomers.length && (
        <Pagination currentPage = {currentPage} itemsPerPage = {itemsPerPage} length = {filteredCustomers.length} onActivePage = {handleActivePage} />
        )}

        </>
    ); 
}
export default CustomerPage; 
import React, {useEffect, useState} from "react"; 
import Pagination from "../components/Pagination";
import invoicesAPI from "../services/invoicesAPI";
import moment from "moment";


const STATUS_CLASSES = {
    PAID: "success",
    SENT: "info",
    CANCELLED: "danger"
};
const STATUS_LABEL = {
    PAID: "Payé",
    SENT: "Envoyé",
    CANCELLED: "Annulé"
}

const InvoicesPage = props => { 

    const [invoices, setInvoices] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState("");


    // Search action (not working voir github)
    const filteredInvoices = invoices.filter( 
        i => 
            i.customer.firstName.toLowerCase().includes(search.toLowerCase()) || 
            i.customer.lastName.toLowerCase().includes(search.toLowerCase()) ||
            i.amount.toLowerCase().includes(search.toLowerCase()) ||
            STATUS_LABEL[i.status].toLowerCase().includes(search.toLowerCase())
        );

    const handleSearch = event => {
        const value  = event.currentTarget.value;
        setSearch(value);
        setCurrentPage(1);
    }


    // Delete action
    const handleDelete = async id => {   
        const originalInvoices = [...invoices];
        setInvoices(invoices.filter(invoice => invoice.id !== id))

        try {
            await invoicesAPI.delete(id)
        } catch (error) {           
            setInvoices(originalInvoices)
            console.log(error.response, "n'a pas ete supprime")
        }    
    }

    // Get all customers   
    const fetchInvoices = async () => {
        try{
            const data = await invoicesAPI.findAll()
            setInvoices(data)
        } catch (error) {
            console.log(error.response)
        } 
    }
      
    useEffect( () => {
        fetchInvoices();            
    }, []);

   

    // // params pagination
    const handleActivePage = page => [
        setCurrentPage(page)
    ];
    const itemsPerPage = 10;
    const paginatedInvoices = Pagination.getData(filteredInvoices, currentPage, itemsPerPage) 
    
    // formattage des dates
    const formatDate = (str) => moment(str).format('DD/MM/YYYY');

    return (
        <>

        <h1> La page des factures </h1>

        <div className="form-group">
            <input type="text" className="form-control" placeholder="Recherchez..." onChange={handleSearch} value={search} />
        </div>

        <table className="table table-hover">
            <thead>
                <tr>
                <th>Facture n°</th>
                <th>Client</th>
                <th>Montant</th>
                <th>Status</th>
                <th className="text-center">Envoyé le : </th>                
                <th></th>
                </tr>
            </thead>

            <tbody>

                {filteredInvoices.map(invoice => 

                        <tr key={invoice.id}>
                            <td>{invoice.chrono}</td>
                            <td> {invoice.customer.firstName} {invoice.customer.lastName} </td>
                            <td>{invoice.amount.toLocaleString()} €</td>
                            
                            <td className="text-center">
                                <span className={"badge badge-lg badge-" + STATUS_CLASSES[invoice.status] }> {STATUS_LABEL[invoice.status] } </span>
                            </td>
                            <td className="text-center"> {formatDate(invoice.sentAt)} </td>
                            <td> 
                                <button  
                                        className="btn btn-sm btn-warning mr-2">
                                            Editer
                                </button> 

                                <button onClick={() => handleDelete(invoice.id)} 
                                        className="btn btn-sm btn-danger">
                                            Supprimer
                                </button> 
                            </td>
                        </tr>
                    )}
            </tbody>
        </table>

        <Pagination currentPage = {currentPage} itemsPerPage = {itemsPerPage} length = {filteredInvoices.length} onActivePage = {handleActivePage} />

        

        </>
    ); 
}
export default InvoicesPage; 
// Javascripts 
import React from "react";
import ReactDOM from "react-dom";
// Composants
import Navbar from "./components/Navbar";
import HomePage from "./pages/HomePage";
import CustomerPage from "./pages/CustomersPage";
import InvoicesPage from "./pages/InvoicesPage";

// Utils
import {HashRouter, Switch, Route} from 'react-router-dom'


// CSS 
require("../css/app.css");



// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

// console.log('Hello Octogone  /js/app.js');

const App = () => {

    return(
        
    <HashRouter>
        <Navbar />

        <main className="container pt-5">

            <Switch>
                <Route path="/invoices" component={InvoicesPage} />
                <Route path="/customers" component={CustomerPage} />
                <Route path="/" component={HomePage} />                
            </Switch>
        </main>

     </HashRouter>
   
    );
};

const rootElement = document.querySelector('#app');
ReactDOM.render(<App></App>, rootElement);

